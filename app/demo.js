(function() {
	
	'use_strict';

	//
	// ROOT SCOPE PROVIDER
	//
	var $http = new XMLHttpRequest(),
		video,
		fps = 1000 / 10,
		runtime,
		buffer={
			currentFrameId:0,
			currentLinePosition:0,
			currentLineStartTime:0,
			currentLineEndTime:0,
			previousLineStartTime:0,
			nextFrameStartTime:null,
		}
		index=[];

	//
	// ASYNC REQUEST
	//
	$http.open("GET", "demo.xml", true);
	$http.send();

	//
	// THIN SHIM LAYER FOR requestAnimFrame
	//
	window.requestAnimFrame = (function(){
		return window.requestAnimationFrame ||
			window.webkitRequestAnimationFrame ||
			window.mozRequestAnimationFrame ||
			function( callback ){
				//
				// FALLBACK
				//
				window.setTimeout(callback, 1000 / 60);
			};
	})();

        	
var test = {
	
	init: function(){

		video = document.getElementById('v');
		
		//video.addEventListener('playing', test.update, false)
		
		$http.onreadystatechange = function(){
		
		    if (this.readyState == 4 && this.status == 200) {
			    
			    //
			    // NO XML VALIDATION
			    //
				test.parse($http.responseXML);
				
				//
				// Keeping the update alive as a real-time interface for 'animating' purpose.
				//
				test.initControls();
				
		    }
			
		}
		
		
	},
	
	initControls: function(){
		
		var output = document.getElementById('output');
		
		video.addEventListener('seeking', function(){
			output.textContent = '';
		});
		
		video.addEventListener('play', test.update);
		
		window.onkeydown = function(e){ 
		
			switch(e.code){
				case 'Space':
					//
					//<startTime> should be outputted when the space key is pressed DOWN 
					//
					if(buffer.currentLineStartTime){
						output.innerHTML = 'Start: '+buffer.currentLineStartTime;
					}
					break;
				case 'Enter':
					//
					//  new <frame> should be outputted when the user presses ENTER
					//	note: this feature request is unclear. 'new' could be anyone of each.
					//	i set this function for 'go to next frame'.

					if(buffer.nextFrameStartTime){
						video.currentTime = test.parseTime(buffer.nextFrameStartTime);
					}
					break;
					
			}
		};
		
		window.onkeyup = function(e){
			

			switch(e.code){
				case 'Space':
					//
					// <endTime> should be outputted when the space key is subsequently let UP
					//
					if(buffer.currentLineEndTime){
						output.innerHTML = 'End: '+buffer.currentLineEndTime;
					}
					break;
			}
			
			
		}
	},
	parse: function(data){

	//
	// PARSE, BUILD CACHE INDEX, PRODUCE
	//
		//
		//	MY METHOD HERE IS TO CREATE A FAST READABLE INDEXED CACHE FOR THE REAL-TIME DATA SEEKER
		//
	    var xml = data,
			lyrics = document.getElementById("lyrics"),
			song = xml.getElementsByTagName("song")[0],
			ALLOC_BUFFER_SIZE=0,
			FRAME_BUFFER,
			LINE_BUFFER;
			
		var $cache = {
			
			//
			// CACHE INDEX PROCEDURE FOR <frame>, <line>
			//
			frame: function(){
				
				//console.dir(FRAME_BUFFER.id);
				
				if(FRAME_BUFFER && FRAME_BUFFER.id){

					index[FRAME_BUFFER.nodeName+FRAME_BUFFER.id] = {
						startTime: new Array(ALLOC_BUFFER_SIZE),
						endTime: new Array(ALLOC_BUFFER_SIZE)
					};
					
					index.length++;
					
					return $cache.create(FRAME_BUFFER);
				}
				
			},
			line: function(){

				if(LINE_BUFFER && LINE_BUFFER.id){
					
					var time = {
							start:LINE_BUFFER.getElementsByTagName('startTime')[0].textContent,
							end:LINE_BUFFER.getElementsByTagName('endTime')[0].textContent
					}
					//
					// WRITE INTO THE ALLOCATED BUFFER INSTEAD OF prototype.push()
					//
					index[FRAME_BUFFER.nodeName+FRAME_BUFFER.id].startTime[LINE_BUFFER.id-1]=time.start;
					index[FRAME_BUFFER.nodeName+FRAME_BUFFER.id].endTime[LINE_BUFFER.id-1]=time.end;
					
					return $cache.create(LINE_BUFFER);
				}
			},
			//
			// CREATE OUTPUT TYPE FROM CACHE BUFFER
			//
			create: function(buffer){
				
				var el = document.createElement('div');
				var props = {
					id: buffer.nodeName+buffer.id,
					class: buffer.nodeName
				}
				for(var k in props){
					el.setAttribute(k, props[k]);
				}				
				return el;
			}
		}
		
		for(var i = 0; i < song.children.length; i++){

			//
			// READING FRAME INTO MEMORY
			//
			FRAME_BUFFER = song.children[i];
			
			//
			// ALLOCATING MEMORY
			//
			ALLOC_BUFFER_SIZE = FRAME_BUFFER.children.length;

			//
			// <frame> LOCAL CACHE PRODUCT
			//
			var cacheFrame = $cache.frame();
			
			for(var x = 0; x < ALLOC_BUFFER_SIZE; x++){
			
				//
				// READING LINES INTO MEMORY
				//
				LINE_BUFFER = FRAME_BUFFER.children[x];

				//
				// <line LOCAL CACHE PRODUCT
				//
				var cacheLine = $cache.line();
				
				//
				// CACHE CONTENT
				//
				
				cacheLine.innerHTML = LINE_BUFFER.getElementsByTagName('text')[0].textContent;
				
			
				//
				// MERGE LINES INTO FRAMES 
				//
				cacheFrame.appendChild(cacheLine);
			
			}
			
			//
			// DISPLAY CONTENT
			// 
			lyrics.appendChild(cacheFrame);

		}
		
		//
		// FLUSH MEMORY BUFFERS
		//
		delete LINE_BUFFER,FRAME_BUFFER;

		buffer.firstLineStartTime = index['frame'+1].startTime[0];
		
	},
	seekIndex: function(ftime){
		
		var match = function(val){
			return val == ftime;
		}		
		
		//
		// i do discovery till match
		// this should help seeking, but should be optimized at larger scale.
		//
		for(var frameId in index){
			
			if(buffer.previousLineStartTime==ftime){
				break;
			}
			
			var lineIndex = index[frameId].startTime.findIndex(match);
			
			if(lineIndex>-1 || test.parseTime(ftime) < test.parseTime(buffer.firstLineStartTime)){
				
				//output.textContent = '';
				//console.log('found new');
				
				var frameIndexKey = Number(frameId.replace('frame', ''))-1;				
				
				buffer.currentLineId = 'line'+(lineIndex+1);
				buffer.currentFrameId = frameId;
				buffer.currentLineStartTime = index[frameId].startTime[lineIndex];
				buffer.currentLineEndTime = index[frameId].endTime[lineIndex];
				buffer.nextFrameStartTime = (frameIndexKey+2>index.length)?null:index['frame'+(frameIndexKey+2)].startTime[Object.keys(index['frame'+(frameIndexKey+2)].startTime)[0]];
				buffer.previousLineStartTime = buffer.currentLineStartTime;

				
				//
				// CLEAN UP HTML FRAMES
				//
				var frames = document.getElementsByClassName('frame');
				
				for(var framekey = 0;framekey<frames.length;framekey++){
					frames[framekey].style.display='none';
				}
				
				//
				// DISPLAY CURRENT HTML FRAME
				//
				document.getElementById(frameId).style.display='block';
				
				var lines = document.getElementsByClassName('line');
				
				//
				// DISPLAY CURRENT LINE
				//
				for(var linekey = 0;linekey<lines.length;linekey++){
					if(linekey<=lineIndex){
						lines[linekey].setAttribute('class', 'line active');
					} else {
						lines[linekey].setAttribute('class', 'line');
					}
				}

				break;

			} 
			
		}
	},
	formatTime: function(ms){
	    
	    //
	    // it works well on high framerate
	    //
	    
	    var t = {
		    	h:Math.floor(ms / 60 / 60),
		    	m:Math.floor(ms / 60),
				s:Math.floor(ms % 60)
			}
		
		t.h = (t.h >= 10) ? t.h : '0' + t.h;
		t.m = (t.m >= 10) ? t.m : '0' + t.m;
		t.s = (t.s >= 10) ? t.s : '0' + t.s;
		
		return t.h + ':' + t.m + ':' + t.s;
		
	},
	parseTime: function(ftime){

		var ms = ftime.split(':');
		return (Number(ms[0])*60*60+Number(ms[1])*60+Number(ms[2]));
				
	},
	update: function(){
		//
		// 60 FPS
		//
		runtime = window.requestAnimFrame(test.update);
		//runtime = window.setTimeout( test.update, fps);
		test.render();

	},
	render: function(){
		
		var time = test.formatTime(video.currentTime);
		test.seekIndex(time);
		prop.textContent = time;
	}
}
document.addEventListener('DOMContentLoaded', test.init);


})();
